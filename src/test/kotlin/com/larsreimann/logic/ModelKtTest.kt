package com.larsreimann.logic

import org.junit.Assert.assertEquals
import org.junit.Test

internal class ModelKtTest {

    @Test
    fun simplifyNegatedFalse() {
        val expression = !Literal.FALSE
        assertEquals(Literal.TRUE, expression.simplify())
    }

    @Test
    fun simplifyNegatedTrue() {
        val expression = !Literal.TRUE
        assertEquals(Literal.FALSE, expression.simplify())
    }

    @Test
    fun simplifyDoubleNegation() {
        val expected = Variable("x")
        val expression = !!Variable("x")
        assertEquals(expected, expression.simplify())
    }

    @Test
    fun simplifyNegatedDisjunction() {
        val expected = !Variable("x") * !Variable("y")
        val expression = !(Variable("x") + Variable("y"))
        assertEquals(expected, expression.simplify())
    }

    @Test
    fun simplifyNegatedConjunction() {
        val expected = !Variable("x") + !Variable("y")
        val expression = !(Variable("x") * Variable("y"))
        assertEquals(expected, expression.simplify())
    }

    @Test
    fun simplifyConjunctionWithFalse() {
        val expression1 = Literal.FALSE * Variable("x")
        assertEquals(Literal.FALSE, expression1.simplify())

        val expression2 = Variable("x") * Literal.FALSE
        assertEquals(Literal.FALSE, expression2.simplify())
    }

    @Test
    fun simplifyConjunctionWithTrue() {
        val expression1 = Literal.TRUE * Variable("x")
        assertEquals(Variable("x"), expression1.simplify())

        val expression2 = Variable("x") * Literal.TRUE
        assertEquals(Variable("x"), expression2.simplify())
    }

    @Test
    fun simplifyConjunctionWithSameOperands() {
        val expression = Variable("x") * Variable("x")
        assertEquals(Variable("x"), expression.simplify())
    }

//    @Test
//    fun simplifyConjunctionWithOppositeOperands() {
//        val expression1 = Variable("x") * !Variable("x")
//        assertEquals(Literal.FALSE, expression1.simplify())
//
//        val expression2 = !Variable("x") * Variable("x")
//        assertEquals(Literal.FALSE, expression2.simplify())
//    }

    @Test
    fun simplifyDisjunctionWithTrue() {
        val expression1 = Literal.TRUE + Variable("x")
        assertEquals(Literal.TRUE, expression1.simplify())

        val expression2 = Variable("x") + Literal.TRUE
        assertEquals(Literal.TRUE, expression2.simplify())
    }

    @Test
    fun simplifyDisjunctionWithFalse() {
        val expression1 = Literal.FALSE + Variable("x")
        assertEquals(Variable("x"), expression1.simplify())

        val expression2 = Variable("x") + Literal.FALSE
        assertEquals(Variable("x"), expression2.simplify())
    }

    @Test
    fun simplifyDisjunctionWithSameOperands() {
        val expression = Variable("x") + Variable("x")
        assertEquals(Variable("x"), expression.simplify())
    }

//    @Test
//    fun simplifyDisjunctionWithOppositeOperands() {
//        val expression1 = Variable("x") + !Variable("x")
//        assertEquals(Literal.TRUE, expression1.simplify())
//
//        val expression2 = !Variable("x") + Variable("x")
//        assertEquals(Literal.TRUE, expression2.simplify())
//    }
}