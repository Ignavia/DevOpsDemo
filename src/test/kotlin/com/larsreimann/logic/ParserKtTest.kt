package com.larsreimann.logic

import org.junit.Assert.assertEquals
import org.junit.Test

internal class ParserKtTest {

    @Test
    fun createFalseLiteral() {
        val actual = createExpression("0")
        assertEquals(Literal.FALSE, actual)
    }

    @Test
    fun createTrueLiteral() {
        val actual = createExpression("1")
        assertEquals(Literal.TRUE, actual)
    }

    @Test
    fun createVariable() {
        val actual = createExpression("x")
        assertEquals(Variable("x"), actual)
    }

    @Test
    fun createNegation() {
        val actual = createExpression("!1")
        assertEquals(!Literal.TRUE, actual)
    }

    @Test
    fun createDisjunction() {
        val expected = Literal.TRUE + Variable("y")
        val actual = createExpression("1 +y")
        assertEquals(expected, actual)
    }

    @Test
    fun createConjunction() {
        val expected = Variable("z") * Literal.FALSE
        val actual = createExpression("z* 0")
        assertEquals(expected, actual)
    }

    @Test
    fun createComplexExpression() {
        val expected = !(Literal.TRUE + (Variable("x") + Literal.FALSE) * Variable("y"))
        val actual = createExpression("!(1 + (x + 0) * y)")
        assertEquals(expected, actual)
    }
}