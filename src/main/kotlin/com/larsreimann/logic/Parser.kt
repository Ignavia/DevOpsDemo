package com.larsreimann.logic

private fun tokenize(s: String): List<String> {
    val result = mutableListOf<String>()
    var currentWord = ""
    for (c in s) {
        when (c) {
            '!', '(' -> {
                result += c.toString()
            }
            '0', '1' -> {
                if (currentWord == "") {
                    result += c.toString()
                } else {
                    currentWord += c
                }
            }
            ')', '+', '*' -> {
                if (currentWord != "") {
                    result += currentWord
                    currentWord = ""
                }
                result += c.toString()
            }
            ' ' -> {
                if (currentWord != "") {
                    result += currentWord
                    currentWord = ""
                }
            }
            else -> currentWord += c
        }
    }
    if (currentWord != "") {
        result += currentWord
    }
    return result
}

private class Parser(private val tokens: List<String>) {
    var current = 0

    fun parse(): Expression {
        current = 0
        return expression()
    }

    private fun expression(): Expression {
        return disjunction()
    }

    private fun disjunction(): Expression {
        var left = conjunction()
        while (current < tokens.size && tokens[current] == "+") {
            current++
            val right = conjunction()
            left = Disjunction(left, right)
        }
        return left
    }

    private fun conjunction(): Expression {
        var left = negation()
        while (current < tokens.size && tokens[current] == "*") {
            current++
            val right = negation()
            left = Conjunction(left, right)
        }
        return left
    }

    private fun negation(): Expression {
        if (tokens[current] == "!") {
            current++
            val expression = negation()
            return Negation(expression)
        } else {
            return primary()
        }
    }

    private fun primary(): Expression {
        val expression = when (tokens[current]) {
            "0" -> Literal.FALSE
            "1" -> Literal.TRUE
            "(" -> {
                current++
                val expression = expression()
                if (current >= tokens.size || tokens[current] != ")") {
                    throw IllegalArgumentException("Expected a closing parenthesis.")
                }
                expression
            }
            else -> Variable(tokens[current])
        }
        current++
        return expression
    }
}

fun createExpression(s: String): Expression {
    val tokens = tokenize(s)
    return Parser(tokens).parse()
}