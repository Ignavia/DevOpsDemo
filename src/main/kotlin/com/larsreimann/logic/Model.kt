package com.larsreimann.logic

interface Expression {
    fun simplify() = this

    operator fun plus(other: Expression): Expression {
        return Disjunction(this, other)
    }

    operator fun times(other: Expression): Expression {
        return Conjunction(this, other)
    }

    operator fun not(): Expression {
        return Negation(this)
    }
}

enum class Literal : Expression {
    FALSE {
        override fun toString() = "0"
    },
    TRUE {
        override fun toString() = "1"
    }
}

data class Variable(val name: String) : Expression {
    override fun toString() = name
}

data class Disjunction(val left: Expression, val right: Expression) : Expression {
    override fun toString() = "$left + $right"

    override fun simplify(): Expression {
        val newLeft = left.simplify()
        val newRight = right.simplify()
        return when {
            newLeft == Literal.TRUE || newRight == Literal.TRUE -> Literal.TRUE
            newLeft == Literal.FALSE -> newRight
            newRight == Literal.FALSE -> newLeft
            newLeft == newRight -> newLeft
//            (!newLeft).simplify() == newRight -> Literal.TRUE
            else -> newLeft + newRight
        }
    }
}

data class Conjunction(val left: Expression, val right: Expression) : Expression {
    override fun toString() = "${left.withOptionalParentheses()} * ${right.withOptionalParentheses()}"

    private fun Expression.withOptionalParentheses() =
            when (this) {
                is Disjunction -> "($this)"
                else -> this.toString()
            }

    override fun simplify(): Expression {
        val newLeft = left.simplify()
        val newRight = right.simplify()
        return when {
            newLeft == Literal.FALSE || newRight == Literal.FALSE -> Literal.FALSE
            newLeft == Literal.TRUE -> newRight
            newRight == Literal.TRUE -> newLeft
            newLeft == newRight -> newLeft
//            (!newLeft).simplify() == newRight -> Literal.FALSE
            else -> newLeft * newRight
        }
    }
}

data class Negation(val expression: Expression) : Expression {
    override fun toString() = "!${expression.withOptionalParentheses()}"

    private fun Expression.withOptionalParentheses() =
            when (this) {
                is Disjunction, is Conjunction -> "($this)"
                else -> this.toString()
            }

    override fun simplify() =
            when (expression) {
                Literal.FALSE -> Literal.TRUE
                Literal.TRUE -> Literal.FALSE
                is Negation -> expression.expression.simplify()
                is Disjunction -> (!expression.left * !expression.right).simplify()
                is Conjunction -> (!expression.left + !expression.right).simplify()
                else -> !(expression.simplify())
            }
}