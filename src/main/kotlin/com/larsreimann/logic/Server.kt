package com.larsreimann.logic

import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.request.receiveText
import io.ktor.response.respondText
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

fun main(args: Array<String>) {
    embeddedServer(Netty, 8090) {
        routing {
            post("/logic/simplify") {
                val text = call.receiveText()
                val expression = createExpression(text)
                val simplifiedExpression = expression.simplify()
                call.respondText(simplifiedExpression.toString(), ContentType.Text.Plain)
            }
        }
    }.start(wait = true)
}